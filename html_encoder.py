__author__ = "Edward J. Stembler <mailto:ejstembler@mac.com>"
__copyright__ = "Copyright (c) 2009, Edward J. Stembler"
__license__ = "MIT"
__date__ = "2009-02-18"
__module_name__ = "Html Encoder"
__version__ = "1.0"
version_info = (1,0,0)


import clr

clr.AddReference("System.Windows.Forms")
clr.AddReference("System.Web")
clr.AddReference("System.Drawing")

from System.Windows.Forms import *
from System.Web import HttpUtility
from System.Drawing import *

import System
from System import *


class FormMain(Form):
    
    def __init__(self):
        
        self.InitializeComponent()

    
    def InitializeComponent(self):
        
        self._textBoxInput = TextBox()
        self._textBoxOutput = TextBox()
        self._groupBoxType = GroupBox()
        self._radioButtonDecode = RadioButton()
        self._radioButtonEncode = RadioButton()
        self._checkBoxCopy = CheckBox()
        self._buttonAction = Button()
        self._groupBoxType.SuspendLayout()
        self.SuspendLayout()
        #
        # _textBoxInput
        #
        self._textBoxInput.Font = Font("Consolas", 10.5, FontStyle.Regular, GraphicsUnit.Point, 0)
        self._textBoxInput.Location = Point(12, 12)
        self._textBoxInput.Multiline = True
        self._textBoxInput.Name = "textBoxInput"
        self._textBoxInput.ScrollBars = ScrollBars.Both
        self._textBoxInput.Size = Size(380, 103)
        self._textBoxInput.TabIndex = 0
        #
        # _textBoxOutput
        #
        self._textBoxOutput.Font = Font("Consolas", 10.5, FontStyle.Regular, GraphicsUnit.Point, 0)
        self._textBoxOutput.Location = Point(12, 125)
        self._textBoxOutput.Multiline = True
        self._textBoxOutput.Name = "textBoxOutput"
        self._textBoxOutput.ReadOnly = True
        self._textBoxOutput.ScrollBars = ScrollBars.Both
        self._textBoxOutput.Size = Size(380, 103)
        self._textBoxOutput.TabIndex = 1
        #
        # _groupBoxType
        #
        self._groupBoxType.Controls.Add(self._radioButtonEncode)
        self._groupBoxType.Controls.Add(self._radioButtonDecode)
        self._groupBoxType.FlatStyle = FlatStyle.System
        self._groupBoxType.Location = Point(12, 234)
        self._groupBoxType.Name = "groupBoxType"
        self._groupBoxType.Size = Size(168, 51)
        self._groupBoxType.TabIndex = 2
        self._groupBoxType.TabStop = False
        self._groupBoxType.Text = "Type"
        #
        # _radioButtonDecode
        #
        self._radioButtonDecode.AutoSize = True
        self._radioButtonDecode.FlatStyle = FlatStyle.System
        self._radioButtonDecode.Location = Point(18, 19)
        self._radioButtonDecode.Name = "radioButtonDecode"
        self._radioButtonDecode.Size = Size(69, 18)
        self._radioButtonDecode.TabIndex = 0
        self._radioButtonDecode.Text = "Decode"
        self._radioButtonDecode.UseVisualStyleBackColor = True
        self._radioButtonDecode.CheckedChanged += self.radioButtonType_CheckedChanged
        #
        # _radioButtonEncode
        #
        self._radioButtonEncode.AutoSize = True
        self._radioButtonEncode.Checked = True
        self._radioButtonEncode.FlatStyle = FlatStyle.System
        self._radioButtonEncode.Location = Point(93, 19)
        self._radioButtonEncode.Name = "radioButtonEncode"
        self._radioButtonEncode.Size = Size(68, 18)
        self._radioButtonEncode.TabIndex = 1
        self._radioButtonEncode.TabStop = True
        self._radioButtonEncode.Text = "Encode"
        self._radioButtonEncode.UseVisualStyleBackColor = True
        self._radioButtonEncode.CheckedChanged += self.radioButtonType_CheckedChanged
        #
        # _checkBoxCopy
        #
        self._checkBoxCopy.AutoSize = True
        self._checkBoxCopy.Checked = True
        self._checkBoxCopy.CheckState = CheckState.Checked
        self._checkBoxCopy.FlatStyle = FlatStyle.System
        self._checkBoxCopy.Location = Point(186, 254)
        self._checkBoxCopy.Name = "checkBoxCopy"
        self._checkBoxCopy.Size = Size(115, 18)
        self._checkBoxCopy.TabIndex = 3
        self._checkBoxCopy.Text = "Copy to Clipboard"
        self._checkBoxCopy.UseVisualStyleBackColor = True
        #
        # _buttonAction
        #
        self._buttonAction.FlatStyle = FlatStyle.System
        self._buttonAction.Location = Point(317, 250)
        self._buttonAction.Name = "buttonAction"
        self._buttonAction.Size = Size(75, 23)
        self._buttonAction.TabIndex = 4
        self._buttonAction.Text = "Encode"
        self._buttonAction.UseVisualStyleBackColor = True
        self._buttonAction.Click += self.buttonAction_Click
        #
        # FormMain
        #
        self.AcceptButton = self._buttonAction;
        self.AutoScaleDimensions = SizeF(6.0, 13.0)
        self.AutoScaleMode = AutoScaleMode.Font
        self.ClientSize = Size(406, 299)
        self.Controls.Add(self._buttonAction)
        self.Controls.Add(self._checkBoxCopy)
        self.Controls.Add(self._groupBoxType)
        self.Controls.Add(self._textBoxOutput)
        self.Controls.Add(self._textBoxInput)
        self.FormBorderStyle = FormBorderStyle.FixedDialog;
        self.MaximizeBox = False;
        self.Name = "FormMain"
        self.StartPosition = FormStartPosition.CenterScreen;
        self.Text = "Html Encoder"
        self._groupBoxType.ResumeLayout(False)
        self._groupBoxType.PerformLayout()
        self.ResumeLayout(False)
        self.PerformLayout()

    
    def radioButtonType_CheckedChanged(self, *args):
        
        self._buttonAction.Text = args[0].Text

    
    def buttonAction_Click(self, *args):
        
        actions = { True: HttpUtility.HtmlEncode, False: HttpUtility.HtmlDecode }
        
        self._textBoxOutput.Text = actions[self._radioButtonEncode.Checked](self._textBoxInput.Text)
        
        if self._checkBoxCopy.Checked:
            Clipboard.SetText(self._textBoxOutput.Text)


if __name__ == "__main__":
    Application.EnableVisualStyles()
    Application.SetCompatibleTextRenderingDefault(False)
    Application.Run(FormMain())